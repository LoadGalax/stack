/* Tarea - 1 */
const array = [1, 2, 3, 115, 4, 5, 6, 7, 8];
const highestNumber = Math.max(...array);
console.log(highestNumber)


/* Tarea - 2 */
const KM = '10000000'

if (KM <= 1000) {
    console.log('Pie')
} else if (KM <= 10000) {
    console.log('Bicicleta')
} else if (KM <= 30000) {
    console.log('Colectivo')
} else if (KM <= 100000) {
    console.log('Auto')
} else if (KM >= 100000) {
    console.log('Avion')
}