import React from 'react';
import ReactDOM from 'react-dom';
/* import './index.css'; */
/* import App from './App'; */
import Nav from './comp/Nav';
/* import BodyLeft from './comp/BodyLeft';
import BodyRight from './comp/BodyRight'; */
import MainBody from './comp/MainBody'
/* import reportWebVitals from './reportWebVitals'; */


ReactDOM.render(
  <React.StrictMode>
    <Nav />
    <MainBody />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
/* reportWebVitals(); */
