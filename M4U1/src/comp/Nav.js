import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';

function Navbootstrap() {
  const handleSelect = (eventKey) => alert(`selected ${eventKey}`);

  return (
    
    <Nav fill variant="pills" activeKey="1" onSelect={handleSelect} className="navbar navbar-expand-lg navbar-dark bg-dark">
      <Nav.Item>
        <Nav.Link eventKey="1" href="#/home" className="fas fa-gamepad">
        &nbsp;Games
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link eventKey="2" title="Item">
          WIP
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link eventKey="3" disabled>
          WIP
        </Nav.Link>
      </Nav.Item>
      <NavDropdown title="Extra" id="nav-dropdown">
        <NavDropdown.Item eventKey="4.1">Action</NavDropdown.Item>
        <NavDropdown.Item eventKey="4.2">Another action</NavDropdown.Item>
        <NavDropdown.Item eventKey="4.3">Something else here</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item eventKey="4.4">Separated link</NavDropdown.Item>
      </NavDropdown>
    </Nav>
   
  );
}



export default Navbootstrap